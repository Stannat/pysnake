import pygame
import game.gf as gf
import game.fieldObjects.snakeTile as st
import game.fieldObjects.snakePath as sp
import game.fieldObjects.empty as empt
import game.fieldObjects.fruit as fr
import game.fieldObjects.wall as w
import game.agent as agent
from game.snake import EDirection


def draw_gf(screen):
    tile_size = gf.getTileSize()
    border = 2
    l_path_tiles = gf.getSnakePathsPos()
    tile_whole_screen = [tile_size, tile_size,
                         (tile_size * gf.getFieldSize()[1]) - (2 * tile_size) + border,
                         (tile_size * gf.getFieldSize()[0]) - (2 * tile_size) + border]
    pygame.draw.rect(screen, gf.dColors['WHITE'], tile_whole_screen)
    for y in range(gf.getFieldSize()[0]):
        for x in range(gf.getFieldSize()[1]):
            tile_rect = [x * tile_size, y * tile_size, tile_size, tile_size]
            tile_rect_inner = [tile_rect[0] + border, tile_rect[1] + border, tile_rect[2] - border,
                               tile_rect[3] - border]
            if isinstance(gf.field[y][x], empt.Empty):
                pygame.draw.rect(screen, gf.dColors['GREY'], tile_rect_inner)
            if agent.is_on:
                for l_p_tiles in l_path_tiles:
                    for p_tile in l_p_tiles:
                        if (y, x) == p_tile.getPos():
                            p_tile.draw(screen)

            if isinstance(gf.field[y][x], w.Wall):
                pygame.draw.rect(screen, gf.dColors['BLACK'], tile_rect_inner)
            elif isinstance(gf.field[y][x], st.SnakeTile):
                pygame.draw.rect(screen, gf.field[y][x].getColor(), tile_rect_inner)
            elif isinstance(gf.field[y][x], fr.Fruit):
                pygame.draw.rect(screen, gf.dColors['GREEN'], tile_rect_inner)


def displayStatus(screen):
    tile_size = gf.getTileSize()

    # display score
    score_table_bg_width = tile_size * 6
    score_table_bg_height = tile_size * (len(gf.getScores()) + 3)  # +1 for caption
    # x and y positions
    txt_pos = [(gf.getFieldSize()[1] + 1) * tile_size, 1 * tile_size]
    tile_rect = [txt_pos[0], txt_pos[1], score_table_bg_width, score_table_bg_height]
    pygame.draw.rect(screen, gf.dColors['GREEN'], tile_rect)
    # select the font to use, size, bold, italics
    font = pygame.font.SysFont('Calibri', 23, True, False)
    # "True" for anti-aliased text
    text_autoplay = font.render(f'Autoplay: {agent.is_on}', True, gf.dColors['BLACK'])
    screen.blit(source=text_autoplay, dest=[txt_pos[0] + 3, txt_pos[1]])
    text_score_caption = font.render(f'Snake Scores', True, gf.dColors['BLACK'])
    screen.blit(source=text_score_caption, dest=[txt_pos[0] + 3, txt_pos[1] + tile_size])
    font = pygame.font.SysFont('Calibri', 18, True, False)
    for inx, score in enumerate(gf.getScores()):
        # x and y positions
        txt_pos = [(gf.getFieldSize()[1] + 1) * tile_size, (inx + 3) * tile_size]
        text_score = font.render(f'Snake {gf.snakes[inx].getColorName()}: {score}', True, gf.dColors['BLACK'])
        screen.blit(source=text_score, dest=[txt_pos[0] + 3, txt_pos[1]])

    if gf.isGameOver():
        txt_pos = [(gf.getFieldSize()[1] + 1) * tile_size, 0 * tile_size]
        tile_rect = [txt_pos[0], txt_pos[1], score_table_bg_width, tile_size]
        pygame.draw.rect(screen, gf.dColors['RED'], tile_rect)

        font = pygame.font.SysFont('Calibri', 25, True, False)
        text_game_over = font.render("Game Over", True, gf.dColors['BLACK'])
        screen.blit(source=text_game_over, dest=[txt_pos[0] + 3, txt_pos[1]])


def main():
    gf.init()
    pygame.init()
    scr_w, scr_h = 700, 500
    screen_size = (scr_w, scr_h)
    screen = pygame.display.set_mode(screen_size)
    pygame.display.set_caption("Snake")
    # snakes
    snake_red = gf.snakes[0]
    if gf.getSnakesAmount() > 1:
        snake_blue = gf.snakes[1]

    # Loop until the user clicks the close button or presses 'q'- key.
    done = False

    # Used to manage how fast the screen updates
    clock = pygame.time.Clock()

    # Update Loop
    while not done:
        # region Events
        # events like key presses and closing event
        for event in pygame.event.get():  # User did something
            if event.type == pygame.QUIT:  # If user clicked close
                done = True  # Flag that we are done so we exit this loop
            elif event.type == pygame.KEYDOWN:

                if event.key == pygame.K_q:
                    done = True
                if event.key == pygame.K_m:
                    agent.is_on = not agent.is_on
                # controls for first snake
                elif event.key == pygame.K_UP:
                    snake_red.setSnakeDir(EDirection.UP)
                elif event.key == pygame.K_RIGHT:
                    snake_red.setSnakeDir(EDirection.RIGHT)
                elif event.key == pygame.K_DOWN:
                    snake_red.setSnakeDir(EDirection.DOWN)
                elif event.key == pygame.K_LEFT:
                    snake_red.setSnakeDir(EDirection.LEFT)
                # controls for second snake
                if gf.getSnakesAmount() > 1:
                    if event.key == pygame.K_w:
                        snake_blue.setSnakeDir(EDirection.UP)
                    elif event.key == pygame.K_d:
                        snake_blue.setSnakeDir(EDirection.RIGHT)
                    elif event.key == pygame.K_s:
                        snake_blue.setSnakeDir(EDirection.DOWN)
                    elif event.key == pygame.K_a:
                        snake_blue.setSnakeDir(EDirection.LEFT)
        # endregion

        # region Game logic
        if not gf.isGameOver():
            if agent.is_on:
                for snake in gf.snakes:
                    agent.setNextStep(is_greedy=False, snake=snake)
            gf.tryMove()
        # endregion

        # region Drawing
        # fill screen with a background color before drawing if necessary
        # screen.fill(gf.WHITE)
        draw_gf(screen)
        displayStatus(screen)

        # screen update
        pygame.display.flip()
        # limit to x frames per second
        clock.tick(60)
        # endregion
    pygame.quit()


if __name__ == '__main__':
    main()
