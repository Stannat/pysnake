import time

import game.gf as gf
import game.fieldObjects.fruit as fr
import game.fieldObjects.empty as empt
import game.fieldObjects.snakePath as sp
import game.treeNode as tn
from game.fieldObjects.TwoDPoint import TwoDPoint
from game.snake import EDirection

is_on = False


def calcNextPositions(cur_pos):
    assert isinstance(cur_pos, TwoDPoint)

    pos_top = TwoDPoint(cur_pos.y - 1, cur_pos.x)
    pos_right = TwoDPoint(cur_pos.y, cur_pos.x + 1)
    pos_bot = TwoDPoint(cur_pos.y + 1, cur_pos.x)
    pos_left = TwoDPoint(cur_pos.y, cur_pos.x - 1)
    return [pos_top, pos_right, pos_bot, pos_left]


def calcNextCoordinates(cur_pos):
    assert isinstance(cur_pos, tuple)
    assert len(cur_pos) == 2

    pos_top = (cur_pos[0] - 1, cur_pos[1])
    pos_right = (cur_pos[0], cur_pos[1] + 1)
    pos_bot = (cur_pos[0] + 1, cur_pos[1])
    pos_left = (cur_pos[0], cur_pos[1] - 1)
    return [pos_top, pos_right, pos_bot, pos_left]


def setNextStep(is_greedy, snake):
    snake_pos = (snake.y, snake.x)
    l_next_coordinates = calcNextCoordinates(snake_pos)
    l_next_positions = pruneToFieldBounds(l_next_coordinates)

    if is_greedy:
        next_pos = nextStepGreedy(l_next_positions)
    else:
        dist_to_next_fruit = gf.getMaxTiles()
        start_node = tn.TreeNode(parent=None, depth=0, pos=(snake.y, snake.x),
                                 cost=dist_to_next_fruit)

        next_pos = a_star_nodes(node=start_node, l_aims=gf.fruits, l_valid_fe=[empt.Empty, fr.Fruit], grid=gf.field,
                                max_delta=10)
        drawNodePath(start_node, snake)

    if next_pos[0] < snake_pos[0]:
        snake.setSnakeDir(EDirection.UP)
    elif next_pos[0] > snake_pos[0]:
        snake.setSnakeDir(EDirection.DOWN)
    elif next_pos[1] > snake_pos[1]:
        snake.setSnakeDir(EDirection.RIGHT)
    elif next_pos[1] < snake_pos[1]:
        snake.setSnakeDir(EDirection.LEFT)


def nextStepGreedy(l_next_positions):
    l_valid_fe = [empt.Empty, fr.Fruit]

    distances = dict()
    for pos in l_next_positions:
        item_distance = heuristicForDistance(two_2_grid=gf.field, d_targets=gf.fruits,
                                             start_pos=pos, l_valid_fe=l_valid_fe)
        distances[pos] = item_distance

    if len(distances) > 0:
        min_distance_item = min(distances.items(), key=lambda item: item[1])
        min_item_distance = min_distance_item[0]
    return min_item_distance


def a_star(opened, closed, l_valid_fe, l_aims, grid, start_time, max_time):
    # determine min of closed elements
    min_closed = min(closed.items(), key=lambda item: item[1])
    # remember it as an opened element
    opened[min_closed[0]] = min_closed[1]
    del closed[min_closed[0]]
    # coordinates around the opened element
    l_next_coordinates = calcNextCoordinates(min_closed)
    l_next_positions = pruneToFieldBounds(l_next_coordinates)

    # add new nodes to closed list
    for next_pos in l_next_positions:
        # check if field in grid is a valid / an expandable element
        if next_pos not in opened.keys():
            min_item_distance = heuristicForDistance(two_2_grid=grid, d_targets=gf.fruits,
                                                     start_pos=next_pos, l_valid_fe=l_valid_fe)
            # put the least distance to a fruit in the closed node
            closed[next_pos] = min_item_distance


def a_star_nodes(node, l_valid_fe, l_aims, grid, max_delta):
    # determine min of closed elements
    assert isinstance(node, tn.TreeNode)
    start_time = time.time()
    total_nodes_count = 1
    aim_node = None
    while time.time() * 1000 < start_time * 1000 + max_delta and aim_node is None:
        min_closed, _ = node.getMinChild2(is_closed=True)

        # remember node as an opened element
        min_closed.is_closed = False
        # coordinates around the opened element
        l_next_coordinates = calcNextCoordinates(min_closed.pos)
        l_next_positions = pruneToFieldBounds(l_next_coordinates)

        # add new nodes to closed list
        for next_pos in l_next_positions:
            # check if field in grid is a valid or an expandable element
            if not node.isExistent(next_pos):
                min_item_distance = heuristicForDistance(two_2_grid=grid, d_targets=l_aims,
                                                         start_pos=next_pos, l_valid_fe=l_valid_fe)
                # put the least distance to a fruit in the closed node
                child_node = tn.TreeNode(parent=min_closed, depth=min_closed.depth + 1,
                                         pos=next_pos, cost=min_item_distance)
                min_closed.append(child_node)
                total_nodes_count += 1
                if min_item_distance == 0:
                    aim_node = child_node

    next_node = node.nextLeastCost2()
    return next_node.pos


def drawNodePath(node, snake):
    assert isinstance(node, tn.TreeNode)
    assert isinstance(snake, gf.sn.Snake)
    tile_size = gf.getTileSize()
    # lists begins from nearest point to fruit
    l_pos = node.getPathPositions()
    len_all_pos = len(l_pos)
    l_tiles = list()
    for nr, pos in enumerate(l_pos):
        around = calcNextCoordinates(pos)
        eChosen_tile = None

        # check if next node is the snake head or not
        if nr < len_all_pos - 1:
            next_pos = l_pos[nr + 1]
        else:
            next_pos = snake.getSnakePosTuple()

        # check if first node is not the fruit
        if nr == 0 and pos not in gf.fruits.keys():
            # next pos is top or bottom
            if next_pos == around[0] or next_pos == around[2]:
                eChosen_tile = sp.EPathDirection.vertical
            # next pos is right or left
            elif next_pos == around[1] or next_pos == around[3]:
                eChosen_tile = sp.EPathDirection.horizontal
        elif nr > 0:
            prev_pos = l_pos[nr - 1]
            # next pos is top or bottom
            if prev_pos == around[0] and next_pos == around[2] or next_pos == around[0] and prev_pos == around[2]:
                eChosen_tile = sp.EPathDirection.vertical
            # next pos is right or left
            elif prev_pos == around[1] and next_pos == around[3] or next_pos == around[1] and prev_pos == around[3]:
                eChosen_tile = sp.EPathDirection.horizontal
            elif prev_pos == around[0] and next_pos == around[1] or next_pos == around[0] and prev_pos == around[1]:
                eChosen_tile = sp.EPathDirection.up_right
            elif prev_pos == around[1] and next_pos == around[2] or next_pos == around[1] and prev_pos == around[2]:
                eChosen_tile = sp.EPathDirection.down_right
            elif prev_pos == around[2] and next_pos == around[3] or next_pos == around[2] and prev_pos == around[3]:
                eChosen_tile = sp.EPathDirection.down_left
            elif prev_pos == around[3] and next_pos == around[0] or next_pos == around[3] and prev_pos == around[0]:
                eChosen_tile = sp.EPathDirection.up_left
            else:
                raise ValueError(f'not valid case prev: {prev_pos}, cur: {pos}, next: {next_pos}')
        if eChosen_tile is not None:
            l_tiles.append(sp.SnakePath(pos[0], pos[1], snake.getColor(), eChosen_tile, tile_size))
    snake.setNodePositions(l_tiles)


def heuristicForDistance(two_2_grid, d_targets, start_pos, l_valid_fe):
    min_item_distance = gf.getMaxTiles()
    if any(list(filter(lambda valid_fe: isinstance(two_2_grid[start_pos[0]][start_pos[1]], valid_fe), l_valid_fe))):
        for target_pos, val in d_targets.items():
            # add new minimal distance to a target that was found
            dist_to_next_fruit = manhattan(start_pos, target_pos)
            if dist_to_next_fruit < min_item_distance:
                min_item_distance = dist_to_next_fruit
    return min_item_distance


def pruneToFieldBounds(l_coordinates):
    height, width = gf.getFieldSize()
    return list(filter(lambda pos: 0 <= pos[0] < height and 0 <= pos[1] < width, l_coordinates))


def manhattan(p1, p2):
    assert isinstance(p1, tuple) and len(p1) == 2
    assert isinstance(p2, tuple) and len(p1) == 2
    return abs(p1[0] - p2[0]) + abs(p1[1] - p2[1])
