import random
from enum import Enum

import game.gf as gf
import game.fieldObjects.gameobject as gobject
import game.fieldObjects.snakeTile as st
import game.fieldObjects.fruit as fr
from game.fieldObjects.TwoDPoint import TwoDPoint


class EDirection(Enum):
    UP = 0
    RIGHT = 1
    DOWN = 2
    LEFT = 3


class Snake(gobject.GameObject):
    """Snake class"""
    snakes = 0

    def __init__(self, field, snake_start_pos, e_direction, snake_len, color_name, color):
        self._l_pos = list()
        assert snake_start_pos is not None
        assert isinstance(e_direction, EDirection)
        self._color = color
        self._color_name = color_name
        self._eDirection = e_direction
        self._snake_len = snake_len
        self._tiles = list()
        self._isSated = False
        self._score = 0
        Snake.snakes += 1
        # check if position is free
        while self.__isOccupied(field, snake_start_pos):
            snake_start_pos.y = random.randint(1, len(field) - 1)
            snake_start_pos.x = random.randint(1, len(field[0]) - 1)
        # put snake in array
        super().__init__(snake_start_pos.y, snake_start_pos.x)
        self.__setSnakeField(field, snake_start_pos)

    def __isOccupied(self, field, snake_start_pos):
        for cur_snake_len in range(self._snake_len, 0, -1):
            if field[snake_start_pos.y][snake_start_pos.x + cur_snake_len] is st.SnakeTile:
                return True
        return False

    def __setSnakeField(self, field, snake_start_pos):
        for cur_snake_len in range(1, self._snake_len + 1):
            snake_tile = st.SnakeTile(age=cur_snake_len, pos=snake_start_pos, snake=self)
            # change the x position
            snake_tile.x = snake_start_pos.x + cur_snake_len

            field[snake_tile.y][snake_tile.x] = snake_tile
            self._tiles.append(snake_tile)
        self.x += self._snake_len

    def setNewTile(self, field, pos):
        new_tile = st.SnakeTile(self._snake_len + 1, pos, self)
        if isinstance(field[pos.y][pos.x], fr.Fruit):
            self._isSated = True
            self._score += 1
            gf.deleteFruit(field[pos.y][pos.x])
            gf.createFruit()
            gf.increaseSpeed()
        field[pos.y][pos.x] = new_tile
        self._tiles.append(new_tile)
        self.y = pos.y
        self.x = pos.x

    def getNextDir(self):
        next_field_pos = TwoDPoint(0, 0)

        if self._eDirection == EDirection.UP:
            next_field_pos = TwoDPoint(self.y - 1, self.x)
        elif self._eDirection == EDirection.RIGHT:
            next_field_pos = TwoDPoint(self.y, self.x + 1)
        elif self._eDirection == EDirection.DOWN:
            next_field_pos = TwoDPoint(self.y + 1, self.x)
        elif self._eDirection == EDirection.LEFT:
            next_field_pos = TwoDPoint(self.y, self.x - 1)
        return next_field_pos

    def __isDirOppositeToMovement(self, e_new_direction):
        assert isinstance(e_new_direction, EDirection)
        if e_new_direction == EDirection.UP and self._eDirection == EDirection.DOWN:
            return True
        elif e_new_direction == EDirection.DOWN and self._eDirection == EDirection.UP:
            return True
        elif e_new_direction == EDirection.RIGHT and self._eDirection == EDirection.LEFT:
            return True
        elif e_new_direction == EDirection.LEFT and self._eDirection == EDirection.RIGHT:
            return True
        else:
            return False

    def reduceTiles(self, number=0):
        if not self._isSated:
            del self._tiles[number]
        else:
            self._isSated = False

    def getTail(self):
        return self._tiles[0]

    def getSnakeDir(self):
        return self._eDirection

    def setSnakeDir(self, e_new_direction):
        assert isinstance(e_new_direction, EDirection)
        if not self.__isDirOppositeToMovement(e_new_direction):
            self._eDirection = e_new_direction

    def getSnakePos(self):
        return TwoDPoint(self._tiles[-1].y, self._tiles[-1].x)

    def getSnakePosTuple(self):
        return self._tiles[-1].y, self._tiles[-1].x

    def getScore(self):
        return self._score

    def getColor(self):
        return self._color

    def getColorName(self):
        return self._color_name

    def setNodePositions(self, l_pos):
        self._l_pos = l_pos

    def getNodePositions(self):
        return self._l_pos

    def getTiles(self):
        return self._tiles

    def isInTiles(self, pos):
        return pos in self._tiles
