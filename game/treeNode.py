class TreeNode:
    def __init__(self, parent, depth, pos, cost):
        self.parent = parent
        self.depth = depth
        self.pos = pos
        self.cost = cost
        self.children = list()
        self.is_closed = True

    def getMinChild(self, is_closed):
        min_node = self
        global_min_cost = self.cost
        for next_node in self.children:
            next_min_node, min_cost = next_node.getMinChild(is_closed)
            if min_cost < global_min_cost and next_min_node.is_closed == is_closed:
                global_min_cost = min_cost
                min_node = next_min_node
        return min_node, global_min_cost

    def getMinChild2(self, is_closed):
        min_node = self
        global_min_cost = self.cost
        for next_node in self.children:
            next_min_node, min_cost = next_node.getMinChild2(is_closed)
            if min_cost < global_min_cost and next_min_node.is_closed == is_closed or min_cost <= 1:
                global_min_cost = min_cost
                min_node = next_min_node
        return min_node, global_min_cost

    # finds node with least costs
    def getMinChild3(self):
        min_node = self
        global_min_cost = self.cost
        for next_node in self.children:
            next_min_node, min_cost = next_node.getMinChild3()
            if min_cost < global_min_cost:
                global_min_cost = min_cost
                min_node = next_min_node
        return min_node, global_min_cost

    def isExistent(self, pos):
        if self.pos[0] == pos[0] and self.pos[1] == pos[1]:
            return True
        else:
            for next_node in self.children:
                if next_node.isExistent(pos):
                    return True
        return False

    def append(self, child):
        assert isinstance(child, TreeNode)
        self.children.append(child)

    def nextLeastCost(self):
        min_node, _ = self.getMinChild2(is_closed=True)
        child = None
        while min_node.parent is not None:
            child = TreeNode(None, min_node.depth, min_node.pos, min_node.cost)
            min_node = min_node.parent
        if child is None:
            min_node, _ = self.getMinChild2(is_closed=False)
            while min_node.parent is not None:
                child = TreeNode(None, min_node.depth, min_node.pos, min_node.cost)
                min_node = min_node.parent
        return child

    def nextLeastCost2(self):
        min_node, _ = self.getMinChild3()
        child = self
        while min_node.parent is not None:
            child = TreeNode(None, min_node.depth, min_node.pos, min_node.cost)
            min_node = min_node.parent
        return child

    def getPathPositions(self):
        min_node, _ = self.getMinChild3()
        child = self
        l_path = list()
        while min_node.parent is not None:
            child = TreeNode(None, min_node.depth, min_node.pos, min_node.cost)
            l_path.append(child.pos)
            min_node = min_node.parent
        return l_path
