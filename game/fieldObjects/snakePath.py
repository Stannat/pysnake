import math
from enum import Enum
import pygame
import game.fieldObjects.TwoDPoint as tdp


class EPathDirection(Enum):
    horizontal = 0
    vertical = 1
    up_right = 2
    down_right = 3
    down_left = 4
    up_left = 5
    target = 6


class SnakePath(tdp.TwoDPoint):
    def __init__(self, y, x, color, e_direction, size):
        super().__init__(y, x)
        # check for horizontal, vertical or arrow
        assert isinstance(e_direction, EPathDirection)
        self.color = color
        self.e_direction = e_direction
        self.size = size

    # point of reference for angle is 3 hour (right from center) ccw
    def __drawCircleArc(self, surface, center, start_degree, end_degree):
        (x, y) = center
        radius = max(self.size / 2, 1)
        rect = (x - radius, y - radius, radius * 2, radius * 2)
        start_rad = start_degree / 180.0 * math.pi
        end_rad = end_degree / 180.0 * math.pi

        pygame.draw.arc(surface, self.color, rect, start_rad, end_rad, max(self.size//10, 1))

    def draw(self, surface):
        x = self.x * self.size
        y = self.y * self.size
        if self.e_direction == EPathDirection.horizontal:
            pygame.draw.line(surface, self.color, (x, y+(self.size / 2)),
                             (x+self.size, y+(self.size/2)), self.size//10)
        elif self.e_direction == EPathDirection.vertical:
            pygame.draw.line(surface, self.color, (x + self.size / 2, y),
                             (x + self.size / 2, y + self.size), self.size // 10)
        elif self.e_direction == EPathDirection.up_right:
            self.__drawCircleArc(surface, (x + self.size, y + 1), 180, 270)
        elif self.e_direction == EPathDirection.up_left:
            self.__drawCircleArc(surface, (x+2, y+2), -90, 0)
        elif self.e_direction == EPathDirection.down_right:
            self.__drawCircleArc(surface, (x + self.size, y + self.size), 90, 180)
        elif self.e_direction == EPathDirection.down_left:
            self.__drawCircleArc(surface, (x+2, y + self.size), 0, 90)
        else:
            pygame.draw.rect(surface, self.color, (x + max(self.size/3, 1), y + max(self.size/3, 1),
                                                   x + max(self.size/3*2, 1), y + max(self.size/3*2, 1)))

    def getPos(self):
        return self.y, self.x
