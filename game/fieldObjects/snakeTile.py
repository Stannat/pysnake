import game.fieldObjects.gameobject as gobject
import game.snake as sn


class SnakeTile(gobject.GameObject):
    def __init__(self, age, pos, snake):
        assert isinstance(snake, sn.Snake)
        super().__init__(pos.y, pos.x)
        self.snake = snake
        self.age = age

    def getColor(self):
        return self.snake.getColor()
