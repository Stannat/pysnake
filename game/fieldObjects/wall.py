import game.fieldObjects.gameobject as gobject


class Wall(gobject.GameObject):

    def __init__(self, y, x):
        super().__init__(y, x)