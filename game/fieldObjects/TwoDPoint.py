
class TwoDPoint:
    def __init__(self, y, x):
        self.y = y
        self.x = x

    def euclidean(self, point):
        assert isinstance(point, TwoDPoint)
        return ((self.y - point.y)**2 + (self.x - point.x)**2)**0.5

    def manhattan(self, point):
        assert isinstance(point, TwoDPoint)
        return abs(self.y - point.y) + abs(self.x - point.x)
