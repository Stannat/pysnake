import game.fieldObjects.gameobject as gobject


class Fruit(gobject.GameObject):

    def __init__(self, y, x):
        super().__init__(y, x)
        self.score = 1
