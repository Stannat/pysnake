import math


import numpy
from enum import Enum
import random
import game.snake as sn
import game.fieldObjects.snakeTile as st
import game.fieldObjects.wall as w
import game.fieldObjects.fruit as fr
import game.fieldObjects.empty as empt
from game.fieldObjects.TwoDPoint import TwoDPoint
import time


class EFieldType(Enum):
    EMPTY = 0
    WALL = 1
    SNAKE = 2
    FRUIT = 3


# colors
dColors = dict()
dColors['BLACK'] = (0, 0, 0)
dColors['WHITE'] = (255, 255, 255)
dColors['BLUE'] = (0, 0, 255)
dColors['GREEN'] = (0, 255, 0)
dColors['RED'] = (255, 0, 0)
dColors['GREY'] = (128, 128, 128)

_field_width: int = 20
_field_height: int = 10
_field_tile_size = 25
field = [[]]
_isGameOver = False

snakes = list()
_snake_len = 3
_snake_start_pos = TwoDPoint(_field_height // 2, _field_width // 2 - _snake_len // 2)
_snake_start_pos2 = TwoDPoint(_field_height // 3, _field_width // 3 - _snake_len // 2)
fruits = dict()


def init():
    global field
    field = createFields(_field_height, _field_width)
    # define snake position
    snakes.append(sn.Snake(field, _snake_start_pos, sn.EDirection.RIGHT, _snake_len, 'RED', dColors['RED']))
    snakes.append(sn.Snake(field, _snake_start_pos2, sn.EDirection.RIGHT, _snake_len, 'BLUE', dColors['BLUE']))
    time_delta = 1000
    num_of_speed_steps = ((_field_width - 2) * (_field_height - 2) - (_snake_len * len(snakes))) // (len(snakes) * 10)
    # sigmoid function for a flat start, steep mid and flat end function progression
    speed_steps = [int((time_delta / (1 + math.exp(-x)))) for x in numpy.linspace(-4.0, 4.0, num_of_speed_steps)]

    # init random for random fruit positions
    random.seed(time.time())
    # calculate check and calculate fruit position
    createFruit()

    # time variables
    tryMove.time_delta = time_delta
    tryMove.time_start = time.time()
    increaseSpeed.min_step = speed_steps[0]
    increaseSpeed.speed_steps = reversed(speed_steps)
    tryMove.current_step = next(increaseSpeed.speed_steps)


def getTileSize():
    return _field_tile_size


def getFieldSize():
    return _field_height, _field_width


def getMaxTiles():
    return _field_height * _field_width


def createFields(field_height, field_width):
    _field = list()
    for y in range(field_height):
        row = list()
        for x in range(field_width):
            if y == 0 or y == field_height - 1:
                row.append(w.Wall(y, x))
            elif x == 0 or x == field_width - 1:
                row.append(w.Wall(y, x))
            else:
                row.append(empt.Empty(y, x))
        _field.append(row)
    return _field


def createFruit():
    fruit_pos = TwoDPoint(0, 0)
    while True:
        fruit_pos.y = random.randint(1, _field_height - 2)
        fruit_pos.x = random.randint(1, _field_width - 2)
        if isinstance(field[fruit_pos.y][fruit_pos.x], empt.Empty):
            fruit = fr.Fruit(fruit_pos.y, fruit_pos.x)
            fruits[(fruit_pos.y, fruit_pos.x)] = fruit
            field[fruit_pos.y][fruit_pos.x] = fruit
            break


def deleteFruit(fruit):
    assert isinstance(fruit, fr.Fruit)
    del fruits[(fruit.y, fruit.x)]


def tryMove():
    global _isGameOver
    tryMove.time_end = time.time()

    if tryMove.time_end * 1000 - tryMove.time_start * 1000 > tryMove.current_step:
        if not _isGameOver and canMove():
            for snake in snakes:
                tail_tile = snake.getTail()
                assert isinstance(tail_tile, st.SnakeTile)
                field[tail_tile.y][tail_tile.x] = empt.Empty(tail_tile.y, tail_tile.x)
                snake.reduceTiles()
        else:
            _isGameOver = True
        tryMove.time_start = time.time()


def increaseSpeed():
    tryMove.current_step = next(increaseSpeed.speed_steps, increaseSpeed.min_step)


def isGameOver():
    return _isGameOver


def canMove():
    for snake in snakes:
        assert isinstance(snake, sn.Snake)
        next_field_pos = snake.getNextDir()
        next_field = field[next_field_pos.y][next_field_pos.x]
        if not isinstance(next_field, fr.Fruit) and not isinstance(next_field, empt.Empty):
            return False
        snake.setNewTile(field, next_field_pos)
    return True


def getScores():
    scores = list()
    for snake_nr in range(len(snakes)):
        snake = snakes[snake_nr]
        assert isinstance(snake, sn.Snake)
        scores.append(snake.getScore())
    return scores


def getSnakePathsPos():
    l_paths = list()
    for snake in snakes:
        l_paths.append(snake.getNodePositions())
    return l_paths


def getSnakesAmount():
    return len(snakes)
