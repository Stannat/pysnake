# PySnake

Snake implementation with A* pathfinding in Python and Pygame<br>
![Ingame Screenshot of two AI controlled snakes](./screenshot.png#center "Ingame Screenshot")

**Run**

(requires that python path is defined)
- python main.py

**Controls**

- Player: Red snake
   - Move left and right: ← and →
   - Move up and down: ↑ or ↓

- Player: Blue snake
  - Move left and right: "a" and "d"
  - Move up and down: "w" and "s"

- Activate AI and lay back: "m"
- Quit and close game: "q"
